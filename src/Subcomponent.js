import React, { Component } from 'react';

import SimpleState from 'react-simple-state';

const simpleState = new SimpleState();

class Subcomponent extends Component {
    constructor(props) {
        super(props);

        const user = simpleState.getState('user');

        this.state = {
            user: user
        }
    }

    componentWillMount() {
        simpleState.subscribe('user', this, (nextUser) => {
            this.setState({
                user: nextUser
            });
        });
    }

    componentWillUnmount() {
        simpleState.unsubscribe('user', this);
    }

    updateToDwight() {
        simpleState.evoke('user', {
            first: 'Dwight',
            last: 'Schrute',
            job: 'Beet Farmer'
        });
    }

    render() {
        return (
            <p className="App-intro App-border"><a href="javascript:void(0);" onClick={this.updateToDwight}>Switch to Dwight Schrute</a></p>
        );
    }
}

export default Subcomponent;
