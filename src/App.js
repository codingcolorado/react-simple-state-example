import React, { Component } from 'react';
import './App.css';
import Subcomponent from './Subcomponent';

import SimpleState from 'react-simple-state';

const simpleState = new SimpleState();

class App extends Component {
    constructor(props) {
        super(props);

        simpleState.addListener('user', {
            first: 'Bob',
            last: 'Vance',
            job: 'Vance Refridgeration'
        });

        const user = simpleState.getState('user');

        this.state = {
            user: user
        }
    }

    componentWillMount() {
        simpleState.subscribe('user', this, (nextUser) => {
            this.setState({
                user: nextUser
            });
        });
    }

    componentWillUnmount() {
        simpleState.unsubscribe('user', this);
    }

    updateInfo() {
        simpleState.evoke('user', {
            first: 'Michael',
            last: 'Scott',
            job: 'Manager at Dunder Mifflin'
        });
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Home Page</h2>
                    <p>Notice that the subcomponent has no props passed down to it, and that when you click the listener, it evokes a new state in the state simpleState, which it's parent component is subscribed to, thus updating its state.</p>
                </div>

                <p className="App-intro">Your name is {this.state.user.first} {this.state.user.last} and your job is {this.state.user.job}.</p>
                <p className="App-intro"><a href="javascript:void(0);" onClick={this.updateInfo}>Switch to Michael Scott</a></p>
                
                <Subcomponent />

                <br /><br /><br />

                <h3>Important Note:</h3>
                <p>Don't forget to subscribe your components in componentWillMount and unsubscribe them in componentWillUnmount</p>
            </div>
        );
    }
}

export default App;
