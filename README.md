# React Simple State Example
an example of how to use react-simple-state

## What this Is

This is a simple example of how to use [React Simple State](https://bitbucket.org/codingcolorado/react-simple-state).

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find the most recent version of the Create React App guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Getting Started

- Install dependencies: `npm install`
- Run the server: `npm start`
- Navigate to the React app: `http://localhost:3000/`
- Click on the links to see the state manager in action




